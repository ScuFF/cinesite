import os
import sys
import tempfile
import re

os.environ["QT_PREFERRED_BINDING"] = os.pathsep.join(["PyQt5", "PySide2", "PySide", "PyQt4"])

from rdialog.Qt import QtCore
from rdialog.Qt import QtGui
from rdialog.Qt import QtWidgets

from rdialog.custom_widgets import Viewer, RenderToolPane
from rdialog.renderer import render


# StreamOI object to replace stdout and stderr streams
class ConsoleStream(QtCore.QObject):
    # Signal emitted when new message comes in stream
    written = QtCore.Signal(str)

    def __init__(self, parent=None):
        super(ConsoleStream, self).__init__(parent)

    def write(self, string):
        self.written.emit(string)

# Rendering thread
# Render running it separate thread featuring Qt threading mechanism
# Communication with main thread happening thorough Qt events system
class RenderThread(QtCore.QThread):
    def __init__(self, image_path, material_color=None):
        QtCore.QThread.__init__(self)
        self.image_path = image_path
        self.material_color = (1.0, 1.0, 1.0)
        if material_color:
            self.material_color = material_color

    def __del__(self):
        self.wait()

    def set_params(self, image_path=None, material_color=None):
        if image_path:
            self.image_path = image_path
        if material_color:
            self.material_color = material_color

    def run(self):
        render(self.image_path, self.material_color)


# Main window for application
# Composed from the widget from rdialog.custom_widgets module
class RenderDialog(QtWidgets.QMainWindow):
    def __init__(self, parent=None):
        super(RenderDialog, self).__init__(parent, QtCore.Qt.Window)
        self.setWindowTitle('Arnold render dialog')
        self.settings = QtCore.QSettings("pfx", "sinecite_renderview")

        # Prepare render thraed
        self.generate_output_filepath()
        self.render_thread = RenderThread(image_path=self.image_path)
        self.render_thread.finished.connect(self.render_finished)
        self.render_thread.started.connect(self.render_started)

        #Root widget
        widget = QtWidgets.QWidget()
        layout = QtWidgets.QHBoxLayout()

        # Image viewer
        self.image_pane = Viewer()

        # Render tool pane
        self.render_toolpane = RenderToolPane()
        self.render_toolpane.setMinimumWidth(300)
        self.render_toolpane.setMaximumWidth(550)

        # Setting up layout
        layout.addWidget(self.image_pane)
        layout.addWidget(self.render_toolpane)
        widget.setLayout(layout)
        self.setCentralWidget(widget)

        # Restoring window position and size from previos run
        # or apply default size and position
        if self.settings.contains("mainWindowGeometry"):
            self.restoreGeometry(self.settings.value("mainWindowGeometry"))
        else:
            desktop_widget = QtWidgets.QDesktopWidget()
            win_height = int(desktop_widget.availableGeometry(self).height() * 3 / 5)
            win_width = int(desktop_widget.availableGeometry(self).width() * 3 / 5)
            self.resize(win_width, win_height)

        # Redirecting console output to render tool pane
        message_stream = ConsoleStream()
        sys.stdout = message_stream
        sys.stderr = message_stream
        # When something comes to message stream call handler
        message_stream.written.connect(self.parse_console)

        # Start render by clicking "Render" button
        self.render_toolpane.render_button.clicked.connect(self.run)

    # Generating filepath in temdir for storing rendered images
    def generate_output_filepath(self):
        temp_dir = tempfile.gettempdir()
        self.image_path = os.path.join(temp_dir, 'image.jpg')

    # Handler which placing bufer string into render log widget
    def parse_console(self, buffer):
        render_log = self.render_toolpane.render_log
        render_log.moveCursor(QtGui.QTextCursor.End)
        render_log.insertPlainText(buffer)
        render_log.moveCursor(QtGui.QTextCursor.End)

        #regexp check if progress bar should update
        regex = re.compile("^ *[0-9]{1,3}\% done - [0-9]* rays\/pixel.*$")
        if regex.match(buffer):
            self.render_toolpane.progressBar.setValue(self.render_toolpane.progressBar.value() + 5)

    # Handler for render tread preparatin
    def run(self):
        if self.render_thread.isRunning():
            self.render_thread.terminate()
        # Get current settings
        self.render_thread.set_params(material_color=self.render_toolpane.color_picker.color_as_float())
        self.render_thread.set_params(image_path=self.image_path)
        # Run the render
        self.render_thread.start()

    # Handler for render log refresh when each new render starting
    def render_started(self):
        self.render_toolpane.render_log.clear()
        self.render_toolpane.render_button.setDisabled(True)
        self.render_toolpane.progressBar.show()
        self.render_toolpane.progressBar.setValue(0)

    # Handler for processing result when render is finished
    def render_finished(self):
        self.image_pane.show_image(self.image_path)
        self.render_toolpane.render_button.setDisabled(False)
        self.render_toolpane.progressBar.reset()
        self.render_toolpane.progressBar.hide()

    # Standard handler for window closing
    def closeEvent(self, event):
        # Remove tem image that was created during the renders
        try:
            os.remove(self.image_path)
        except:
            pass
        # Wait for rendering thread
        self.render_thread.wait()
        # Save size and position of the window
        self.settings.setValue("mainWindowGeometry", self.saveGeometry())
        event.accept()


def main():
    app = QtWidgets.QApplication(sys.argv)
    window = RenderDialog()
    window.show()
    sys.exit(app.exec_())


if __name__ == "__main__":
    main()
