import os

os.environ["QT_PREFERRED_BINDING"] = os.pathsep.join(["PyQt5", "PySide2", "PySide", "PyQt4"])

# noinspection PyUnresolvedReferences
from .Qt import QtCore
# noinspection PyUnresolvedReferences
from .Qt import QtGui
# noinspection PyUnresolvedReferences
from .Qt import QtWidgets


class QHLine(QtWidgets.QFrame):
    def __init__(self):
        super(QHLine, self).__init__()
        self.setFrameShape(QtWidgets.QFrame.HLine)
        self.setFrameShadow(QtWidgets.QFrame.Sunken)

# Color slider widget
# Has slider and spinbox bound to change a value
class ColorSlider(QtWidgets.QWidget):
    color_changed = QtCore.Signal()

    def __init__(self, color_component, initial_value):
        super(ColorSlider, self).__init__()
        layout = QtWidgets.QHBoxLayout()
        layout.setContentsMargins(0, 0, 0, 0)
        label = QtWidgets.QLabel(color_component)
        self.slider = QtWidgets.QSlider(QtCore.Qt.Horizontal)
        self.slider.setRange(0, 255)
        self.slider.setValue(initial_value)
        self.spinbox = QtWidgets.QSpinBox()
        self.spinbox.setRange(0, 255)
        self.spinbox.setValue(initial_value)
        layout.addWidget(label)
        layout.addWidget(self.slider)
        layout.addWidget(self.spinbox)
        self.setLayout(layout)

        self.slider.valueChanged.connect(self.update_spinbox)
        self.spinbox.valueChanged.connect(self.update_slider)

    def get_value(self):
        return self.slider.value()

    def set_value(self, value):
        self.slider.setValue(value)

    # Update spinbox when slider has changed
    # Blocking signal for preventing loop of events
    def update_spinbox(self):
        value = self.slider.value()
        self.spinbox.blockSignals(True)
        self.spinbox.setValue(value)
        self.spinbox.blockSignals(False)
        self.color_changed.emit()

    # Update slider when spinbox has changed
    # Blocking signal for preventing loop of events
    def update_slider(self):
        value = self.spinbox.value()
        self.slider.blockSignals(True)
        self.slider.setValue(value)
        self.slider.blockSignals(False)
        self.color_changed.emit()


# RGB HSV Widget
# Stack of sliders
class Sliders(QtWidgets.QWidget):
    def __init__(self):
        super(Sliders, self).__init__()
        layout = QtWidgets.QVBoxLayout()
        layout.setContentsMargins(0, 0, 0, 0)
        self.red_slider = ColorSlider('R', 120)
        self.green_slider = ColorSlider('G', 90)
        self.blue_slider = ColorSlider('B', 90)
        self.hue_slider = ColorSlider('H', 120)
        self.saturation_slider = ColorSlider('S', 90)
        self.value_slider = ColorSlider('V', 90)
        layout.addWidget(self.red_slider)
        layout.addWidget(self.green_slider)
        layout.addWidget(self.blue_slider)
        layout.addWidget(QHLine())
        layout.addWidget(self.hue_slider)
        layout.addWidget(self.saturation_slider)
        layout.addWidget(self.value_slider)
        self.setLayout(layout)


# Color picker widget
# Combines sliders and color sample
class ColorPicker(QtWidgets.QWidget):
    def __init__(self):
        super(ColorPicker, self).__init__()
        layout = QtWidgets.QHBoxLayout()
        layout.setContentsMargins(0, 0, 0, 0)
        self.color_sample = QtWidgets.QPushButton()
        self.color_sample.setAutoFillBackground(True)
        self.color_sample.setFixedSize(120, 120)
        self.sliders = Sliders()
        layout.addWidget(self.color_sample)
        layout.addWidget(self.sliders)
        self.setLayout(layout)

        # Updating HSV slider on RGB change and vice versa
        # Updating color sample
        self.sliders.red_slider.color_changed.connect(self.rgb_to_hsv)
        self.sliders.green_slider.color_changed.connect(self.rgb_to_hsv)
        self.sliders.blue_slider.color_changed.connect(self.rgb_to_hsv)
        self.sliders.hue_slider.color_changed.connect(self.hsv_to_rgb)
        self.sliders.saturation_slider.color_changed.connect(self.hsv_to_rgb)
        self.sliders.value_slider.color_changed.connect(self.hsv_to_rgb)

        # Set HSV values to match RGB once on start
        self.rgb_to_hsv()

    # Get color in float for render scene representation
    def color_as_float(self):
        red = self.sliders.red_slider.get_value()
        green = self.sliders.green_slider.get_value()
        blue = self.sliders.blue_slider.get_value()
        red = float(red) / float(255)
        green = float(green) / float(255)
        blue = float(blue) / float(255)
        return red, green, blue

    # Update HSV sliders when one of RGB slider has changed
    # Blocking signal for preventing loop of events
    def rgb_to_hsv(self):
        red = self.sliders.red_slider.get_value()
        green = self.sliders.green_slider.get_value()
        blue = self.sliders.blue_slider.get_value()
        color = QtGui.QColor.fromRgb(red, green, blue)
        self.sliders.hue_slider.blockSignals(True)
        self.sliders.saturation_slider.blockSignals(True)
        self.sliders.value_slider.blockSignals(True)
        self.sliders.hue_slider.set_value(color.hsvHue())
        self.sliders.saturation_slider.set_value(color.hsvSaturation())
        self.sliders.value_slider.set_value(color.value())
        self.sliders.hue_slider.blockSignals(False)
        self.sliders.saturation_slider.blockSignals(False)
        self.sliders.value_slider.blockSignals(False)
        # update color sample
        self.update_color()

    # Update RGB sliders when one of HSV slider has changed
    # Blocking signal for preventing loop of events
    def hsv_to_rgb(self):
        hue = self.sliders.hue_slider.get_value()
        saturation = self.sliders.saturation_slider.get_value()
        value = self.sliders.value_slider.get_value()
        color = QtGui.QColor.fromHsv(hue, saturation, value)
        self.sliders.red_slider.blockSignals(True)
        self.sliders.green_slider.blockSignals(True)
        self.sliders.blue_slider.blockSignals(True)
        self.sliders.red_slider.set_value(color.red())
        self.sliders.green_slider.set_value(color.green())
        self.sliders.blue_slider.set_value(color.blue())
        self.sliders.red_slider.blockSignals(False)
        self.sliders.green_slider.blockSignals(False)
        self.sliders.blue_slider.blockSignals(False)
        # update color sample
        self.update_color()

    # Taking RGB value for updating color sample
    def update_color(self):
        red = self.sliders.red_slider.get_value()
        green = self.sliders.green_slider.get_value()
        blue = self.sliders.blue_slider.get_value()
        self.color_sample.setStyleSheet('background-color: rgb({}, {}, {})'.format(red, green, blue))

# Render tool pane widget
# Composition of Render log, Color picker and render button
class RenderToolPane(QtWidgets.QWidget):
    def __init__(self):
        super(RenderToolPane, self).__init__()
        layout = QtWidgets.QVBoxLayout()
        layout.setContentsMargins(0, 0, 0, 0)
        self.render_log = QtWidgets.QTextEdit()
        self.render_log.setTextInteractionFlags(
            QtCore.Qt.TextSelectableByMouse |
            QtCore.Qt.TextSelectableByKeyboard)
        self.render_log.setLineWrapMode(QtWidgets.QTextEdit.NoWrap)
        font = QtGui.QFont('monospace')
        font.setStyleHint(QtGui.QFont.Monospace)
        font.setPointSize(9)
        self.render_log.setFont(font)
        self.progressBar = QtWidgets.QProgressBar()
        self.progressBar.setRange(0,100)
        self.progressBar.setAlignment(QtCore.Qt.AlignHCenter | QtCore.Qt.AlignVCenter)
        self.progressBar.hide()
        # palette = QtGui.QPalette()
        # palette.setColor(QtGui.QPalette.Base, QtGui.QColor(70, 70, 60))
        # palette.setColor(QtGui.QPalette.Text, QtGui.QColor(230, 230, 210))
        # self.render_log.setPalette(palette)
        self.color_picker = ColorPicker()
        self.render_button = QtWidgets.QPushButton('Render')
        layout.addWidget(self.render_log)
        layout.addWidget(self.color_picker)
        layout.addWidget(self.progressBar)
        layout.addWidget(self.render_button)
        self.setLayout(layout)


# Viewport for showing rendered image
# Scroll area with QLabel for showing the picture in it
class Viewer(QtWidgets.QScrollArea):
    def __init__(self):
        super(Viewer, self).__init__()
        self.image_label = QtWidgets.QLabel()
        self.image_label.setBackgroundRole(QtGui.QPalette.Base)
        self.image_label.setSizePolicy(QtWidgets.QSizePolicy.Ignored, QtWidgets.QSizePolicy.Ignored)
        self.image_label.setScaledContents(True)
        self.setBackgroundRole(QtGui.QPalette.Dark)
        self.setWidget(self.image_label)
        self.setAlignment(QtCore.Qt.AlignHCenter | QtCore.Qt.AlignVCenter)
        self.image_label.hide()
        self.scaleFactor = 1.0
        self.pressed = False

    # Loading image from dist to viewport
    def show_image(self, image_path):
        self.hChange = self.horizontalScrollBar().value()
        self.vChange = self.verticalScrollBar().value()
        self.image_label.hide()
        reader = QtGui.QImageReader(image_path)
        image = reader.read()
        self.pixmap = QtGui.QPixmap.fromImage(image)
        self.image_label.setPixmap(self.pixmap)
        self.image_label.setVisible(True)
        self.image_label.adjustSize()
        self.zoomImage()
        self.horizontalScrollBar().setSliderPosition(self.hChange)
        self.verticalScrollBar().setSliderPosition(self.vChange)

    def keyPressEvent(self, event):
        if event.key() == QtCore.Qt.Key_Minus and self.image_label.isVisible():
            self.scaleFactor = self.scaleFactor / 1.2
            self.zoomImage()
        elif event.key() == QtCore.Qt.Key_Plus and self.image_label.isVisible():
            self.scaleFactor = self.scaleFactor * 1.2
            self.zoomImage()
        event.accept()
    
    def wheelEvent(self, event):
        if event.delta() > 0 and self.image_label.isVisible():
            self.scaleFactor = self.scaleFactor * 1.2
            self.zoomImage()
        elif event.delta() < 0 and self.image_label.isVisible():
            self.scaleFactor = self.scaleFactor / 1.2
            self.zoomImage()
            
    def mousePressEvent(self, event):
        self.pressed = True
        self.cursorOrigin = event.globalPos()
        self.sliderHorigin = self.horizontalScrollBar().value()
        self.sliderVorigin = self.verticalScrollBar().value()


    def mouseMoveEvent(self, event):
        if self.pressed:
            delta = self.cursorOrigin - event.globalPos()
            self.horizontalScrollBar().setSliderPosition(self.sliderHorigin + delta.x())
            self.verticalScrollBar().setSliderPosition(self.sliderVorigin + delta.y())

    def mouseReleaseEvent(self, event):
        self.pressed = False

    def zoomImage(self):
        size = self.pixmap.size() * self.scaleFactor
        self.image_label.resize(size)
        zoomPixmap = self.pixmap.scaled(size, QtCore.Qt.KeepAspectRatio, QtCore.Qt.SmoothTransformation)
        self.image_label.setPixmap(zoomPixmap)
