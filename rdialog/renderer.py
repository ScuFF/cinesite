from arnold import *

# Message callback which captures output from arnold and place it into python stdout
# this stout will be redirectet to widget by python
def catch_message(log_mask, message_severity_level, message, tabs):
    print(str(message))


# Render procedure
def render(file_path, mat_color):
    AiBegin()

    # register callback
    call_back = AtMsgCallBack(catch_message)
    AiMsgSetCallback(call_back)

    # All outputs to console
    AiMsgSetConsoleFlags(AI_LOG_ALL)

    # Add sphere
    sph = AiNode("sphere")
    AiNodeSetStr(sph, "name", "mysphere")
    AiNodeSetVec(sph, "center", 0.0, 4.0, 0.0)
    AiNodeSetFlt(sph, "radius", 4.0)

    # Add plane under
    pln = AiNode("polymesh")
    AiNodeSetStr(pln, "name", "myplane")

    nsides = [4]
    AiNodeSetArray(pln, "nsides", AiArrayConvert(len(nsides), 1, AI_TYPE_UINT, (c_uint * len(nsides))(*nsides)))
    vidxs = [0, 1, 3, 2]
    AiNodeSetArray(pln, "vidxs", AiArrayConvert(len(vidxs), 1, AI_TYPE_UINT, (c_uint * len(vidxs))(*vidxs)))
    nidxs = [0, 1, 2, 3]
    AiNodeSetArray(pln, "nidxs", AiArrayConvert(len(nidxs), 1, AI_TYPE_UINT, (c_uint * len(nidxs))(*nidxs)))
    vlist = [-15, 0.0, 15.0, 15.0, 0.0, 15.0, -15.0, 0.0, -15.0, 15.0, 0.0, -15.0]
    AiNodeSetArray(pln, "vlist", AiArrayConvert(len(vlist), 1, AI_TYPE_FLOAT, (c_float * len(vlist))(*vlist)))
    nlist = [0.0, 1.0, 0.0, 0.0, 1.0, 0.0, 0.0, 1.0, 0.0, 0.0, 1.0, 0.0]
    AiNodeSetArray(pln, "nlist", AiArrayConvert(len(nlist), 1, AI_TYPE_FLOAT, (c_float * len(nlist))(*nlist)))

    # Add shader
    shader1 = AiNode("standard_surface")
    AiNodeSetStr(shader1, "name", "myshader1")
    AiNodeSetRGB(shader1, "base_color", mat_color[0], mat_color[1], mat_color[2])
    AiNodeSetFlt(shader1, "specular", 0.05)

    # Shader assigments
    AiNodeSetPtr(sph, "shader", shader1)
    AiNodeSetPtr(pln, "shader", shader1)

    # Add camera
    camera = AiNode("persp_camera")
    AiNodeSetStr(camera, "name", "mycamera")
    AiNodeSetVec(camera, "position", 0.0, 10.0, 35.0)
    AiNodeSetVec(camera, "look_at", 0.0, 3.0, 0.0)
    AiNodeSetFlt(camera, "fov", 45.0)

    # Add lights
        # Point
    light = AiNode("point_light")
    AiNodeSetStr(light, "name", "mylight")
    AiNodeSetVec(light, "position", 15.0, 30.0, 15.0)
    AiNodeSetFlt(light, "intensity", 4500.0) # alternatively, use 'exposure'
    AiNodeSetFlt(light, "radius", 4.0) # for soft shadows

        # Environment
    sk_light = AiNode("skydome_light")
    AiNodeSetStr(sk_light, "name", "skydome")
    AiNodeSetFlt(sk_light, "camera", 0.0)
    AiNodeSetFlt(sk_light, "exposure", -2.0)

    # Set render options
    options = AiUniverseGetOptions()
    AiNodeSetInt(options, "AA_samples", 4)
    AiNodeSetInt(options, "xres", 640)
    AiNodeSetInt(options, "yres", 480)
    AiNodeSetInt(options, "GI_diffuse_depth", 4)
    AiNodeSetPtr(options, "camera", camera)

    # Set output driver
    driver = AiNode("driver_jpeg")
    AiNodeSetStr(driver, "name", "mydriver")
    AiNodeSetStr(driver, "filename", file_path)

    # Set image filter
    filter = AiNode("gaussian_filter")
    AiNodeSetStr(filter, "name", "myfilter")

    # Set up render output
    outputs_array = AiArrayAllocate(1, 1, AI_TYPE_STRING)
    AiArraySetStr(outputs_array, 0, "RGBA RGBA myfilter mydriver")
    AiNodeSetArray(options, "outputs", outputs_array)

    # Render
    AiRender(AI_RENDER_MODE_CAMERA)
    AiEnd()

