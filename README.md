# Test assigment

### PyQt/rendering/installation test:
* download and install Prman or Arnold (prman is free for Non-commercial use, Arnold renders images with watermarks)

* write a PyQt dialog that has the following elements:
    * a view for displaying rendered image
    * a view for displaying output log of the renderer
    * a color picker to change the color of the objects in the scene
    * a render button
* when user clicks render button, the program should use renderer python API to render a sphere/teapot with user-defined color and display image and the log in the UI. Please note that you are not expected to write a custom display driver for prman/arnold - you should simply render to disk and load image into the UI.

### Dependencies
* Arnold SDK
* PyQt or PySide

### Preview

Windows

![windows](https://thumbs.gfycat.com/InfiniteInnocentGourami-size_restricted.gif)

Linux

![Linux](https://img.vim-cn.com/9f/add81b82a2c27347c5b20ce9be33f7e6da599e.png)

### Tested
* Windows, python3.6, PyQt5
* Windows, python3.6, PySide2
* Windows, python2.7, PyQt4
* Linux, python3.6, PyQt5